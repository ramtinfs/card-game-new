package edu.ntnu.idatt2001.cardgame.view;

import edu.ntnu.idatt2001.cardgame.model.DeckOfCards;
import edu.ntnu.idatt2001.cardgame.model.PlayerHand;
import edu.ntnu.idatt2001.cardgame.model.PlayingCard;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * The class represents the graphical user interface for the card game application.
 * The class extends the JavaFX Application class and implements the start() method to create the user interface.
 * The user interface consists of a pane, a background image, and several buttons that allow the
 * user to deal a hand of cards, check the hand for certain properties, and restock the deck.
 *
 * @author Ramtin Samavat
 * @version 14.03.2023
 */
public class CardGameGui extends Application {
  private DeckOfCards deckOfCards;
  private PlayerHand playerHand;
  private final Pane root;

  /**
   * Constructor for creating a CardGameGui object, initializing the deck of cards and root pane.
   */
  public CardGameGui() {
    deckOfCards = new DeckOfCards();
    root = new Pane();
  }

  /**
   * The method lunches the application.
   *
   * @param args an array of command-line arguments for the application.
   */
  public static void main(String[] args) {
    launch(args);
  }

  /**
   * The method creates the scene by calling methods to create the buttons,
   * background image, and scene.
   *
   * @param stage the primary stage for the application.
   */
  @Override
  public void start(Stage stage) {
    createButtonForDealingCards();
    createButtonForCheckingCards();
    createButtonForRestockDeck();
    createBackgroundImage(stage);
    createScene(stage);
  }

  /**
   * The method creates the "Deal hand" button and adds it to the root pane.
   */
  private void createButtonForDealingCards() {
    Button dealHandButton = createButton("Deal hand", 130,
            40, 18, 750, 200, new DealHandHandler());
    root.getChildren().add(dealHandButton);
  }

  /**
   * The method creates the "Check hand" button and adds it to the root pane.
   */
  private void createButtonForCheckingCards() {
    Button checkHand = createButton("Check hand", 130,
            40, 18, 750, 300, new CheckHandHandler());
    root.getChildren().add(checkHand);
  }

  /**
   * The method creates the "Restock deck" button and adds it to the root pane.
   */
  private void createButtonForRestockDeck() {
    Button restoclButton = createButton("Restock deck", 130,
            40, 18, 750, 400, new RestockDeckHandler());
    root.getChildren().add(restoclButton);
  }

  /**
   * The method creates a background image and sets it as the background for the root pane.
   * If the image fails to load, it displays an error alert and exits the program.
   *
   * @param stage the primary stage for the application
   */
  private void createBackgroundImage(Stage stage) {
    try {
      Image backgroundImage = new Image("casino-table.jpg");
      BackgroundSize backgroundSize = new BackgroundSize(stage.getWidth(),
              stage.getHeight(), false, false, true, true);
      BackgroundImage background = new BackgroundImage(backgroundImage,
              BackgroundRepeat.NO_REPEAT,
              BackgroundRepeat.NO_REPEAT,
              BackgroundPosition.CENTER,
              backgroundSize);
      root.setBackground(new Background(background));
    } catch (Exception e) {
      Alert alert = new Alert(Alert.AlertType.ERROR, "Failed to load background image.");
      alert.showAndWait();
      System.exit(0);
    }
  }

  /**
   * The method creates and displays a new JavaFX scene on the provided stage,
   * with a specified width and height, and sets the tile of the stage to "Card game".
   * The scene is set to not be resizable.
   *
   * @param stage the stage on which the scene will be displayed.
   */
  private void createScene(Stage stage) {
    Scene scene = new Scene(root, 1000, 900);
    stage.setScene(scene);
    stage.setResizable(false);
    stage.setTitle("Card game");
    stage.show();
  }

  /**
   * The method creates a new button with the given description,
   * size, font size, position and event handler.
   *
   * @param description the text description of the button.
   * @param sizeV the vertical size of the button.
   * @param sizeV1 the horizontal size of the button.
   * @param fontSize the font size of the button's text.
   * @param layoutX the x-coordinate position of the button.
   * @param layoutY the y-coordinate position of the button.
   * @param event the event handler for when the button is clicked.
   * @return button object.
   */
  private Button createButton(String description, int sizeV, int sizeV1,
                              int fontSize, int layoutX, int layoutY,
                              EventHandler<ActionEvent> event) {
    Button button = new Button();
    button.setText(description);
    button.setPrefSize(sizeV, sizeV1);
    button.setFont(Font.font(fontSize));
    button.setLayoutX(layoutX);
    button.setLayoutY(layoutY);
    button.setOnAction(event);
    return button;
  }

  /**
   * The method creates text information for a Text object.
   *
   * @param text the text object.
   * @param info the text information.
   * @param fontSize the font size of the text.
   * @param layoutX the x-coordinate of the text.
   * @param layoutY the y-coordinate of the text.
   */
  private void createTextInformation(Text text, String info, int fontSize,
                                     int layoutX, int layoutY) {
    root.getChildren().remove(text);
    text.setText(info);
    text.setFont(Font.font(fontSize));
    text.setFill(Color.WHITE);
    text.setLayoutX(layoutX);
    text.setLayoutY(layoutY);
    root.getChildren().add(text);
  }

  /**
   * Inner class that implements the EventHandler interface and handles
   * the "Deal Hand" button's action event.
   */
  private class DealHandHandler implements EventHandler<ActionEvent> {
    Text cards = new Text();

    /**
     * The method handles the "Deal Hand" button click event.
     *
     * @param actionEvent The ActionEvent object representing the "Deal Hand" button's action event.
     */
    @Override
    public void handle(ActionEvent actionEvent) {
      try {
        playerHand = new PlayerHand(deckOfCards.dealHand(5));
      } catch (IllegalArgumentException e) {
        Alert alert = new Alert(Alert.AlertType.WARNING, e.getMessage());
        alert.showAndWait();
      }
      StringBuilder sb = new StringBuilder();
      for (PlayingCard cards : playerHand.getPlayerHand()) {
        sb.append(cards.getAsString()).append(" ");
      }
      createTextInformation(cards, sb.toString(), 20, 400, 300);
    }
  }

  /**
   * Inner class that implements the EventHandler interface and handles
   * the "Check Hand" button's action event.
   */
  private class CheckHandHandler implements EventHandler<ActionEvent> {
    Text descriptionOfSum = new Text();
    Text sumOfCards = new Text();
    Text descriptionOfCards = new Text();
    Text cardsOfHearts = new Text();
    Text flush = new Text();
    Text flushStatus = new Text();
    Text queenOfSpades = new Text();
    Text queenOfSpadesStatus = new Text();

    /**
     * The method handles the "Check Hand" button's action event by calling
     * the methods that display the sum of cards, the cards of hearts, the
     * flush status, and the queen of spades status of the player's hand.
     *
     * @param actionEvent the ActionEvent object representing the "Check Hand" button's action event.
     */
    @Override
    public void handle(ActionEvent actionEvent) {
      displaySumOfCards();
      displayCardsOfHearts();
      displayFlushStatus();
      displayQueenOfSpadesStatus();
    }

    /**
     * The method displays the sum of the cards of the player's hand on the GUI.
     */
    private void displaySumOfCards() {
      createTextInformation(descriptionOfSum, "Sum of the cards: ", 18, 300, 550);
      createTextInformation(sumOfCards, Integer.toString(playerHand.sumOfCards()), 18, 440, 550);
    }

    /**
     * The method displays the cards of hearts in the player's hand on the GUI.
     */
    private void displayCardsOfHearts() {
      createTextInformation(descriptionOfCards, "Cards of hearts: ", 18, 550, 550);
      StringBuilder sb = new StringBuilder();
      for (PlayingCard cards : playerHand.getAllHearts()) {
        sb.append(cards.getAsString()).append(" ");
      }
      String textInfo = sb.isEmpty() ? "No hearts." : sb.toString();
      createTextInformation(cardsOfHearts, textInfo, 18, 680, 550);
    }

    /**
     * The method displays the flush status of the player's hand on the GUI.
     */
    private void displayFlushStatus() {
      createTextInformation(flush, "Flush: ", 18, 300, 610);
      String textInfo = playerHand.checkForFlush() ? "Yes" : "No";
      createTextInformation(flushStatus, textInfo, 18, 355, 610);
    }

    /**
     * The method displays the queen of spades status of the player's hand on the GUI.
     */
    private void displayQueenOfSpadesStatus() {
      createTextInformation(queenOfSpades, "Queen of spades: ", 18, 550, 610);
      String textInfo = playerHand.checkForQueenOfSpades() ? "Yes" : "No";
      createTextInformation(queenOfSpadesStatus, textInfo, 18, 695, 610);
    }
  }

  /**
   * Inner class that implements the EventHandler interface and handles the
   * restocking of the deck of cards.
   */
  private class RestockDeckHandler implements EventHandler<ActionEvent> {

    /**
     * The method handles the restocking of the deck of cards when the button
     * is clicked by creating a new deck of cards and displays a confirmation.
     *
     * @param actionEvent the ActionEvent object representing the "Restock " button's action event
     */
    @Override
    public void handle(ActionEvent actionEvent) {
      deckOfCards = new DeckOfCards();
      Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "The deck of cards has been restocked.");
      alert.showAndWait();
    }
  }
}
