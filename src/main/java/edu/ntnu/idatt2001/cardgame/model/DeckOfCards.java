package edu.ntnu.idatt2001.cardgame.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * The class represents a complete deck of cards.
 *
 * @author Ramtin Samavat
 * @version 14.03.2023
 */
public class DeckOfCards {
  private final List<PlayingCard> cardDeck;
  private final char[] suit = {'S', 'H', 'D', 'C'};
  private final int[] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};

  /**
   * Constructor to make an instance of the class.
   * The constructor initialize the list cardDeck and adds cards.
   */
  public DeckOfCards() {
    cardDeck = new ArrayList<>();
    for (char s : suit) {
      for (int f : face) {
        cardDeck.add(new PlayingCard(s, f));
      }
    }
  }

  /**
   * The method deals random cards from the card deck.
   *
   * @param handSize the size of the hand to be dealt.
   * @return list of cards that make up the player's cards on hand.
   * @throws IllegalArgumentException  if the size of the hand is outside the range.
   */
  public List<PlayingCard> dealHand(int handSize) throws IllegalArgumentException {
    if (handSize < 1 || handSize > cardDeck.size()) {
      throw new IllegalArgumentException("Not enough cards left for desired hand size.");
    }
    List<PlayingCard> cardsOnHand = new ArrayList<>();
    Random random = new Random();
    for (int i = 0; i < handSize; i++) {
      PlayingCard card = cardDeck.get(random.nextInt(cardDeck.size()));
      cardsOnHand.add(card);
      cardDeck.remove(card);
    }
    return cardsOnHand;
  }
}
