package edu.ntnu.idatt2001.cardgame.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * The class represents a player hand.
 *
 * @author Ramtin Samavat
 * @version 14.03.2023
 */
public class PlayerHand {
  private final List<PlayingCard> playerHand;

  /**
   * Constructor to make an instance of the class.
   * The constructor initialize the list playerHand, and 
   *
   * @param givenHand the size of the player hand.
   * @throws NullPointerException if the list imported from the method dealHand is null.
   */
  public PlayerHand(List<PlayingCard> givenHand) throws NullPointerException {
    playerHand = new ArrayList<>();
    playerHand.addAll(Objects.requireNonNull(givenHand, "List cannot be null."));
  }

  /**
   * The method retrieves the list playerHand,
   * which contains the player's cards.
   *
   * @return list of player's card.
   */
  public List<PlayingCard> getPlayerHand() {
    return playerHand;
  }

  /**
   * The method calculates the sum of the cards in the player hand.
   *
   * @return the sum of cards in the player hand.
   */
  public int sumOfCards() {
    return playerHand.stream()
            .mapToInt(PlayingCard::getFace)
            .sum();
  }

  /**
   * The method retrieves all cards which are hearts.
   *
   * @return List of cards which are hearts.
   */
  public List<PlayingCard> getAllHearts() {
    return playerHand.stream()
            .filter(playingCard -> ('H') == playingCard.getSuit())
            .toList();
  }

  /**
   * The method checks if the player hand contains queen of spades.
   *
   * @return boolean depending on if the player hand contains queen of spades.
   */
  public boolean checkForQueenOfSpades() {
    return playerHand.stream()
            .anyMatch(playingCard -> ("S12").equals(playingCard.getAsString()));
  }

  /**
   * The method checks for flush.
   *
   * @return boolean depending on if the player hand contains flush.
   */
  public boolean checkForFlush() {
    return playerHand.stream()
            .allMatch(playingCard -> playerHand.get(0).getSuit() == playingCard.getSuit());
  }
}
