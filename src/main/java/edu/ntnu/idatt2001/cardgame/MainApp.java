package edu.ntnu.idatt2001.cardgame;

import edu.ntnu.idatt2001.cardgame.view.CardGameGui;

/**
 * The class runs the main application.
 *
 * @author Ramtin Samavat
 * @version 14.03.2023
 */
public class MainApp {

  /**
   * The method lunches the application.
   *
   * @param args an array of command-line arguments for the application.
   */
  public static void main(String[] args) {
    CardGameGui.main(args);
  }
}