package edu.ntnu.idatt2001.cardgame;

import edu.ntnu.idatt2001.cardgame.model.PlayerHand;
import edu.ntnu.idatt2001.cardgame.model.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The class tests the PlayerHand class.
 *
 * @author Ramtin Samavat
 * @version 14.03.2023
 */
class PlayerHandTest {
  private final List<PlayingCard> deckOfCards = new ArrayList<>();
  private PlayerHand playerHand;

  @BeforeEach
  void setUp() {
    deckOfCards.add(new PlayingCard('H', 12));
    deckOfCards.add(new PlayingCard('S', 12));
    deckOfCards.add(new PlayingCard('D', 10));
    playerHand = new PlayerHand(deckOfCards);
  }

  @Nested
  @DisplayName("Constructor tests")
  class ConstructorTests {
    @Test
    @DisplayName("Test constructor valid input")
    void testConstructorValidInput() {
      List<PlayingCard> validList = new ArrayList<>();
      validList.add(new PlayingCard('H', 12));
      PlayerHand testConstructor = new PlayerHand(validList);
      assertEquals(validList, testConstructor.getPlayerHand());
    }

    @Test
    @DisplayName("Test constructor invalid input throws NullPointerException")
    void testConstructorInvalidInputThrowsNullPointerException() {
      List<PlayingCard> invalidList = null;
      assertThrows(NullPointerException.class, () -> new PlayerHand(invalidList));
    }
  }

  @Nested
  @DisplayName("Get method tests")
  class GetMethodTests {
    @Test
    @DisplayName("Should get player hand")
    void shouldGetPlayerHand() {
      List<PlayingCard> actualList = playerHand.getPlayerHand();
      assertEquals(3, actualList.size());
    }

    @Test
    @DisplayName("Should get sum of cards")
    void shouldGetSumOfCards() {
      assertEquals(34, playerHand.sumOfCards());
    }

    @Test
    @DisplayName("Should get all hearts")
    void shouldGetAllHearts() {
      assertEquals(1, playerHand.getAllHearts().size());
    }
  }

  @Nested
  @DisplayName("Validation tests")
  class ValidationTests {
    @Test
    @DisplayName("Check for queen of spades true")
    void checkForQueenOfSpadesTrue() {
      assertTrue(playerHand.checkForQueenOfSpades());
    }

    @Test
    @DisplayName("Check for queen of spades false")
    void checkForQueenOfSpadesFalse() {
      List<PlayingCard> modifiedDeck = new ArrayList<>(deckOfCards);
      modifiedDeck.removeIf(card -> ("S12").equals(card.getAsString()));
      PlayerHand modifiedHand = new PlayerHand(modifiedDeck);
      assertFalse(modifiedHand.checkForQueenOfSpades());
    }

    @Test
    @DisplayName("Check for flush true")
    void checkForFlushTrue() {
      List<PlayingCard> flush = new ArrayList<>();
      flush.add(new PlayingCard('H', 10));
      flush.add(new PlayingCard('H', 9));
      flush.add(new PlayingCard('H', 7));
      flush.add(new PlayingCard('H', 6));
      flush.add(new PlayingCard('H', 4));
      PlayerHand flushHand = new PlayerHand(flush);
      assertTrue(flushHand.checkForFlush());
    }

    @Test
    @DisplayName("Check for flush false")
    void checkForFlushFalse() {
      assertFalse(playerHand.checkForFlush());
    }
  }
}