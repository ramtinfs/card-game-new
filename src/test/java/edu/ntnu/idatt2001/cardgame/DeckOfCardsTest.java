package edu.ntnu.idatt2001.cardgame;

import edu.ntnu.idatt2001.cardgame.model.DeckOfCards;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * The class tests the DeckOfCards class.
 *
 * @author Ramtin Samavat
 * @version 14.03.2023
 */
class DeckOfCardsTest {
  private DeckOfCards deckOfCards;

  @BeforeEach
  void setUp() {
    deckOfCards = new DeckOfCards();
  }

  @Test
  @DisplayName("Should deal hand")
  void shouldDealHand() {
    assertEquals(5, deckOfCards.dealHand(5).size());
  }

  @Test
  @DisplayName("Should not deal hand throws IllegalArgumentException")
  void shouldNotDealHandThrowsIllegalArgumentException() {
    assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(55));
  }

}