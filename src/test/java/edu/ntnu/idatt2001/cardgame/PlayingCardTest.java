package edu.ntnu.idatt2001.cardgame;

import edu.ntnu.idatt2001.cardgame.model.PlayingCard;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * The class tests the PlayingCard class.
 *
 * @author Ramtin Samavat
 * @version 14.03.2023
 */
class PlayingCardTest {

  private PlayingCard playingCard;

  @BeforeEach
  void setUp() {
    playingCard = new PlayingCard('H', 13);
  }

  @Test
  @DisplayName("Test constructor")
  void testConstructor() {
    PlayingCard testConstructor = new PlayingCard('S', 5);
    assertEquals('S', testConstructor.getSuit());
    assertEquals(5, testConstructor.getFace());
  }

  @Test
  @DisplayName("Should get playing card as string")
  void shouldGetPlyingCardAsString() {
    String expectedString = "H13";
    String actualString = playingCard.getAsString();
    assertEquals(expectedString, actualString);
  }

  @Test
  @DisplayName("Should get suit")
  void shouldGetSuit() {
    char expectedSuit = 'H';
    char actualSuit = playingCard.getSuit();
    assertEquals(expectedSuit, actualSuit);
  }

  @Test
  @DisplayName("Should get face")
  void shouldGetFace() {
    int expectedFace = 13;
    int actualFace = playingCard.getFace();
    assertEquals(expectedFace, actualFace);
  }
}